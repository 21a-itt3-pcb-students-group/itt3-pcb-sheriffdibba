# Week 34
## INTRODUCTION AND CIRCUIT DECISION
Activities
<p>During this week key activities were conducted.<p> 
- Gitlab Project Created
- I made thorough research on what circuit i would like to design and build.
- I chose the circuit of my choose and made further research about that circuit.
- From the research results, i made a block diagram for my circuit.
- I applied for the renewal of my Orcad student license.
- 

Lessons Learned
- Uderstanding the overview of PCB design and manufacturing process.
- I used different websites to look for different circuit ideas for the PCB project.
- Watched few vidoes of PCB manufacturing and design and the circuit i chose.
 


### Smart Key Finder
<p>In this project i am going to work on creating a simple IoT-based smart Key chain.
 Quite often people lose their keys with the believe that they must have left them somewhere far.
 After long thorough search we end up finding out that it is infact couple of metres from arms length.
 The introduction of IoT-based smart key chain with in-built buzzer which will alert you.<p>

 #### How it works
 <p>In case if you can’t find your keys and you remember that you have attached an IoT keychain to your keys, so you take out your phone and open   Chrome and open your Keychain Webpage. Then you click on the toggle button, and in moments, you hear a beep sound coming from your keychain and with this, you can easily track your keys.<p>

 #### Components Required


- ESP8266-01
- AMS1117 3.3V Voltage Regulator
- Buzzer
- Lithium Polymer battery
- 2× 10µf Capacitor


#### Other Hard and Software Required to run the program

- Smart phone
- Wireless Internet connection (Hotspot, home wifi etc)

#### What's a PCB?
PCB stands for printed circuit board but may also be called "Printed wiring boards". Before the emerge of the PCB, circuits were construsted through
a laboriuos process of point-to-point wiring. AS a result there were significant number of failures at the wire jucntions and short circuits when 
wire insulation began to age and crack.
As electronics moved from vacuum tubes and relays to silicon and integrated circuits (ICs), the size and the cost of electronic components began to decrease massivley. There become the pressure to reduce the size and manufacturing costs and electronic products made manufactureres to look for better solutions. Hence the era of PCB began.
#### Description
A PCB is a board that has lines and pads that connect the various connectors and components to each other. A PCB allows signals and power to be routed between physical devices. Solder is the metal that makes the electrical connections between the surface of the PCB and the electronic components. 
#### Composition
- Silkscreen
- Soldermask
- Copper
- Subrate (FR4)
##### Silkscreen (layer 4)
The white silkscreen layer is applied on top of thr soldermask layer. The silkscreen adds letters, numbers, and symbols to the PCB that allow for easier assembly and indicators for humans to better understand the board. Silkscreen labels is often use to indicate what function of each pin or LED. Silkscreen is mostly white color, other colors such as black, gray, red, and yellow are widely used.
##### Soldermask (Layer 3)
The layer on top of the copper foil is called the soldermask layer. This layer gives the PCB its green(sparkFun, red) color. It is overlaid onto the copper layer to insulate the copper traces from accidental contact with other metal, solder, or conductive bits. This layer helps the user to solder to the correct places and prevent solder jumpers.
##### Copper (layer 2)
The copper layer is the thin copper foil, which is laminated to the board with the heat and adhesive. Double sided PCBs, copper is applied to both sides of the substrate. 
##### Substrate FR4 (layer 1)
This is the base material, or the sbstrate, is usually fiberglass. The most common designator for this fiberglass is FR4. The solid core gives the PCB its rigidity and thickness. However there are also flexible PCBs built on flexible high temperature plastic.
# Week 35

Activities
- Norcads beginner course completed.
- Schematic, simulation and PCB layout exercises.
- Orcad capture.
- PCB editor.
During this i watched the vidoes for nordcad beginner course, and made my schematic and did the simulation.
## Schematic Capture with CIS
- Start Orcad capture CIS
- Start a new project
- I added all the components i needed.
- I used the wire symbol to make the connections between components.
- I saved the project and give it a name.
## NORCAD AND SCHEMATIC SIMULATION
- Once the schematic is drawn and cleaned up, i created a simulation profile.
- The simulation is ran at this point.

# Week 36
## SMT Component packages, component research, footprints
Read about the SMT components and aquired knowledge about the appearance and sizes of SMT components. I aso made a research to see different 
SMT components look in real life. I made a documentation of my findings with appropiate coverage of key parameters.

I was able to identified the needed componenets for my project design. i made a thorough research to look into all the relevant technical details 
including datasheets for the needed components and upload them into my gitlab project.

I was able to find out all the relevant cost for the components and the associated shipment costs for the PCB project.

From the lessons taught in the class and in conjuction with couple of tutorial videos i was able to used different websites to select and assign 
footprints to my schematic. DIGI-KEY and mouser were really useful in getting those information. And finally after couple practices i was able 
create my own footprints or use existing footprints.

### Prepare for manufacture
i cleaned up the schematic and prepare it for maunfacturing. i annotated the entire circuit properly. added title to the circuit.
#### Attching package symbols (footprints)
I added prexisting footprints to my design using the library. I also looked into how to modify a footprint.
#### Creation of netlist
This is where the nodes are defined. Express PCB netlist and the versions. The part IDs Table is stated, Net Names Table and Net Connections Table.

### Setting up the PCB design environment
### Layout

### Generating artwork (Gerber files) and dill files
After the completion of the final process, the artwork files are generated they are also called Gerber files. The provide board manufacurer some indication as to how they should be laying out the copper and how they should be drilling board.
These are the files that are going to be sent to the manufacturer for production. There is also the drill files, this tells the manufacurer where to drill holes and also how big the holes should be.
### Generating documentation
### Submitting PCB files for fabrication check


# Week 37
## Netlist 
During this week lessons learned such as how to make netlist, design rule check and making board outline.
The creation of netlist to helps to make a collection of two or more interconnected components. It thus help to identify all my electronic 
components attached to the various nodes.
## Design rule check
The introduction Design Rule Check was used during this phase of the project to help detect and eliminate errors. The video and the lectures
help to view and correct design rule violations.
## Board outline
With the help of the Video and lecture, i was able to set up design parameters and grid spacing, drew the board outline and add layers to the 
design.
# Week 38
## Nordcad workshop
This workshop helps with the basic understanding of PCB schematic designing, running circuit simulation. 
Questions were raised with regards to: 
- when having problems with your login. 
- consistenlty getting crashed out of the system. 
- The software persistently generating errors.
# Week 39
## Design for manufacturing workshop
The Workshop really help to gain the understanding of how PCBs are designed, processed and manufactured. 
### Requirements
- What is a good design
- Why is it important
- What is consider in the design
- What do you need to know prior to design

### Design Considerations
- Functionality
- Cost
- Environment
- Reliability
- Service
- Performance
- Standards
- Legislation

### Standards vs Legislation
International
National
Company
- EN/DS
- IPC
- CCC
- CE
- UL

### What is What
- Machine Directive 2006/42/EC
- EMC Directive 2014/30/EU
- Low Voltage Directive 2014/35/ECL
- Rotts Directive 2011/05/EU

Standards are not not Laws - unless the law says so
Directives are laws

### Basic Knowledge
#### Soldering
- Which alloy
- Melting temperature
- Which process
- Double sided
- Reflow, wave soldering, hand soldering - or all 3.
- Rohs Complaint
- Future Environment

#### What is a good soldering?
- Mechanical Stability and Electrical Stability

### PCB
- Can be extremely complexed
- Not an offshelf component
- Fragile component
- Very supplier dependant
- Process dependant design
- On stock life time(surface treatment) 
- Standards

Example from printline

### What PCB?
- FR2 Phenol/Paper
- FR3 Epoxy/Paper
- FR4/21 Epoxy/Glass fibre 110-150 degrees celcius
- FR4/24 Epoxy/Glass fibre 150-200 degrees celcius
- FR5 Epoxy/Glass fibre 135-185 degrees celcius
- Others e.g. Polyimide 250 degrees celcius

- Tg = Glass Transition
- CTE = Coefficient of thermal expansion
- ECR = Energy Charge Request
- ECN = Engineer Charge Notice

### Specification Example:
- Contract
- Drawings
- IPC

### Testing
- Why do we test?
- What to specify?
- who specifies?
- How to test?

### Documentation
- Schematic 
- BOM
- PCB Layout
- Purchase descriptions
- Test methods 




# Week 44
## Factory design constraints, Board layout, Design layers
During this phase of the project with the help of the video tutorial and the research i made on my own, i was able to design my board layout.
I used square shape as my board layout, and use measurements from as close as possbible to the original board where i took the project idea from.
I designed the borad based on 
- Base Material 
- Number of layers
- Board dimensions

# Week 45
## Routing, gerber and drill files, ordering PCB’s
Understand how routing functionality works and be able to do autoroute on the PCB board.
Understand what the manuafurer requirements are and using best practices to achieve this.
Understand quality manufacturing files.

# Week 46
## Documentation (BOM, schematics, other design files)
Knowledge was obtained on how to create bill of materials and pdf schematic.
# Week 47
## PCB Assembly and test
Thorough knowledge was obtained on how to assemble and solder the PCB board parts, this include both Through-hole components and SMD components.
understand how to conduct the testing of the PCB board after component placement.


